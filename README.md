
# usage

prometheus 
> http://127.0.0.1:8081/actuator/prometheus

swagger
> http://127.0.0.1:8081/swagger-ui/index.html#

initial gauge values (from prometheus)
```
    # HELP myGauge
    # TYPE myGauge gauge
    myGauge{application="micrometer",} 0.0
    # HELP myGaugeWithBuilder
    # TYPE myGaugeWithBuilder gauge
    myGaugeWithBuilder{application="micrometer",tag1="valueOfTag1",tag2="valueOfTag2",} 0.0
```


post to update gauge values
> http://127.0.0.1:8081/micrometer/gaugeWithBuilder?value=6
> http://127.0.0.1:8081/micrometer/gauge?value=2


refresh prometheus page
```
    # HELP myGauge
    # TYPE myGauge gauge
    myGauge{application="micrometer",} 2.0
    # HELP myGaugeWithBuilder
    # TYPE myGaugeWithBuilder gauge
    myGaugeWithBuilder{application="micrometer",tag1="valueOfTag1",tag2="valueOfTag2",} 6.0
```