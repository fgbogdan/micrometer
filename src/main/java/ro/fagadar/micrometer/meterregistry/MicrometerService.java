package ro.fagadar.micrometer.meterregistry;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@Data
public class MicrometerService {
    private final MeterRegistry meterRegistry;

    private final AtomicInteger gaugeValueForBuilder = new AtomicInteger(0);

    private final AtomicInteger gaugeValue;

    @Autowired
    public MicrometerService(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;

        List<Tag> gaugeTags = new ArrayList<>();
        gaugeTags.add(Tag.of("tag1", "valueOfTag1"));
        gaugeTags.add(Tag.of("tag2", "valueOfTag2"));

        Gauge.builder("myGaugeWithBuilder", gaugeValueForBuilder, AtomicInteger::intValue).tags(gaugeTags).strongReference(true)
            .register(meterRegistry);

        this.gaugeValue = this.meterRegistry.gauge("myGauge", new AtomicInteger(0));

    }
}
